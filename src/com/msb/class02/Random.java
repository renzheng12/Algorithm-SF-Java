package com.msb.class02;

/**
 * @Auther: jiudianliu
 * @Date: 2024/1/18 - 01 - 18 - 20:18
 * @Description: com.msb.class02
 * @version: 1.0
 */
public class Random {



    // Random 返回一个[0-1) 的等概率的随机数
    public static void main(String[] args) {
        int testTimes = 100000 ;
        int count = 0 ;
        double x = 0.3 ;
        for (int i = 0 ; i < testTimes ; i++){
            if (xToPower2()< x){
                count++;
            }
        }
        System.out.println((double) count / (double) testTimes);
        // pow 函数  返回X的几次方
        System.out.println(Math.pow( x , 2 ));

        // 如果下边是取最小值函数，依旧想把X的概率调整为X的平方，那么就1-（1-X）的平方就会得到X平方的概率
        //System.out.println((double) 1- Math.pow( (double)(1-x) , 2 ));
    }

    public static  double  xToPower2(){
        // 要想把0-X的概率X调整为X的平方，那么就调两次随机方法，取最大值
        return Math.max( Math.random() , Math.random());

        // 如果下边是取最小值函数，依旧想把X的概率调整为X的平方，那么就1-（1-X）的平方就会得到X平方的概率
        //return Math.min(Math.random() , Math.random());
    }
}
