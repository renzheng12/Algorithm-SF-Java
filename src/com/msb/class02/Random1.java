package com.msb.class02;

/**
 * @Auther: jiudianliu
 * @Date: 2024/1/20 - 01 - 20 - 16:22
 * @Description: com.msb.class02
 * @version: 1.0
 */
public class Random1 {

    /*
    * 得到一个a-b返回等概率返回的一个随机整数的方法
    * 用该方法获得一个x-y等概率返回一个随机整数的方法
    *
    * 本案例为由已知的1-5随机  要求变为  0-6随机
    * */

    public static void main(String[] args) {
        int testTime = 1000000 ;
        int[] counts = new int[8] ;
        for (int i = 0 ; i <= testTime ; i++){
            int num = f3() ;
            //counts[num]++ ;
            counts[num] = counts[num] + 1 ;
        }
        for (int i = 0 ; i < 8 ; i++){
            System.out.println(i + "这个数，出现了" + counts[i] + "次");
        }
        System.out.println();
    }

    // 已知方法f1，1-5 之间 等概率返回一个整数
    public static int f1(){
        return (int) (Math.random() * 5) + 1 ;
    }

    // 根据f1函数生成0和1的等概率发生器
    // f1 如果生成1和2，f2返回0
    // f1 如果生成4和5，f2返回1
    // f1 如果生成3，while重做
    public static int f2(){
        int ans = 0 ;
        do {
            ans = f1();
            // 得到3就重做
        }while ( ans == 3 );
        return ans < 3 ? 0 : 1 ;
    }
    // 由 1-5随机  获得0-6随机
    // f3 是 1-6 二进制占三位 110
    // 所以调三次f2函数，然后将他移到相应的位置,得到的是0-7随机
    public static int f3(){
        return ( f2() << 2 ) + ( f2() << 1 ) + ( f2() << 0 );
    }

    // 1-6的等概率随机   f3方法 遇7重做
    public static int f4(){
        int num1;
        do {
             num1 = f3();
             // 得到7就重做
        }while ( num1 == 7) ;
        return num1;
    }

    /*
    * 已知X以固定概率（返回0的概率为P，那么返回1的概率为1-P）返回0和1，但X的内容看不到
    * 要求等概率返回1
    * */
    // 已知方法P
    public static int P() {
        return Math.random() < 0.73 ? 0 : 1 ;
    }

    // 利用已知方法P  等概率返回0和1
    public static int P1(){
        // 设置一个变量num
        int num = 0 ;
        do {
            // 调用已知方法P
            num = P() ;
            // 如果出现num等于P（）就重做 1 1 或 0 0
            // 一直到 1 0 或 0 1 返回 0 或 1
        }while (num == P()) ;
        return num ;
    }

}
