package com.msb.class04;

import javax.swing.text.View;

/**
 * @Auther: jiudianliu
 * @Date: 2024/12/6 - 12 - 06 - 14:22
 * @Description: com.msb.class04
 * @version: 1.0  单链表实现队列跟栈
 */
public class LinkedListToQueueAndStack {
    // 单链表节点
    public static class Node<V>{
        public V value ;
        public Node<V> next ;

        public Node(V v) {
            value = v;
            next = null;
        }
    }

    // 实现队列  先进先出
    public static class MyQueue<V>{
        // 队列头
        private Node<V> head ;
        // 队列尾
        private Node<V> tail ;
        // 队列数
        private int size ;
        public MyQueue(){
            head = null ;
            tail = null ;
            size = 0 ;
        }
        // 判断队列是否为空
        public boolean isEmpty(){
            return size == 0 ;
        }

        // 返回队列里节点数量
        public int size(){
            return size ;
        }

        // 加入节点  从尾进去
        public void offer(V value){
            // 先 创建节点
            Node<V> node = new Node<V>(value) ;
            // 判断是不是为空  如果tail为空就证明新加的是第一个
            // 那么head=他 ，tail也=他
            if (tail == null){
                head = node ;
            }else {
                // 如果不是 就将tail的下一个指向他
                tail.next = node ;
            }
            // 将tail等于新的Node
            tail = node ;
            size++ ;
        }

        // 取出节点，并将该节点弹出  从头弹出
        public V poll() {
            // 记录弹出节点的数值
            V ans = null ;
            if (head != null){
                // head的值赋值给ans
                ans = head.value ;
                // head移动到下一位
                head = head.next ;
                // 数量减一个
                size-- ;
            }
            // 如果错过了head越过tail，那么将tail也置为空，此时队列为空
            if (head == null) {
                tail = null ;
            }
            return ans ;
        }

        // 查看头结点的值
        public V peak(){
            V ans = null ;
            if (head != null){
                ans = head.value ;
            }
            return ans ;
        }
    }
    // 实现栈  先进后出
    public static class Stack<V>{
        // 头
        private Node<V> head ;
        // 数
        private int size ;
        public Stack(){
            head = null ;
            size = 0 ;
        }
        // 判断队列是否为空
        public boolean isEmpty(){
            return size == 0 ;
        }

        // 返回队列里节点数量
        public int size(){
            return size ;
        }

        // 加入节点
        public void push(V value){
            // 先 创建节点
            Node<V> node = new Node<V>(value) ;
            // 判断是不是为空  如果tail为空就证明新加的是第一个
            // 那么head=他 ，tail也=他
            if (head == null){
                head = node ;
            }else {
                // 如果不是 就将tail的下一个指向他
                head.next = node ;
                head = node ;
            }
            size++ ;
        }

        // 取出节点，并将该节点弹出
        public V pop() {
            // 记录弹出节点的数值
            V ans = null ;
            if (head != null){
                // head的值赋值给ans
                ans = head.value ;
                // head移动到下一位
                head = head.next ;
                // 数量减一个
                size-- ;
            }
            // 如果错过了head越过tail，那么将tail也置为空，此时队列为空
            return ans ;
        }

        // 查看头结点的值
        public V peak(){
            return head != null ? head.value : null ;
        }
    }


    public static void main(String[] args) {

    }
}
